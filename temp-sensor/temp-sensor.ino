#include <TimerOne.h>
// based values code from here https://www.tutorialspoint.com/arduino/arduino_temperature_sensor.htm
// https://playground.arduino.cc/Code/Timer1/ Interrupt found here
float temp = 0;
int tempPin = 0;
unsigned long currentTime;
bool readTemps = true;
void setup() {
   Serial.begin(9600);
   Timer1.initialize(10000000); // Set the Timer to run every 10 Secs
   Timer1.attachInterrupt(readTempsISR); // Added Interrupt
}

void readTempsISR() {
  temp = analogRead(tempPin);
  temp = temp * 0.48828125 * 1.8 + 32;
}

void loop() {
  currentTime = millis() / 1000;
  // Display the currentTime
  Serial.print(currentTime); 
  Serial.print(" , "); 
  Serial.print(temp); // display temperature value
  Serial.println();
  delay(10000);
}
