# en.605.715.81.21.project2

How to Run
First install TimerOne to Arudino by going to sketch > Include Libary > Liberary Manger and Serach for TimerOne
Load the temp-sensor.ino into the the Arudino IDE and push the code.

To collect the data in csv form update the capture-temp-readings.sh file with the the Arudino serial port and run it.
You should see a readings.csv file to parse the data. 